import React from "react";
import "./Header.scss";

const Header = () => (
    <div className="header">
        <h2>Chat App With Golang and reactJs</h2>
    </div>
);

export default Header;