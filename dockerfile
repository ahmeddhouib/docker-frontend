# pull the base image
FROM node:alpine

# set the working direction
WORKDIR /docker-frontend

# add `/app/node_modules/.bin` to $PATH
ENV PATH /docker-frontend/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./

COPY package-lock.json ./

RUN npm install
RUN yarn add node-sass
# add app
COPY . ./

# start app
CMD ["npm", "start"]


